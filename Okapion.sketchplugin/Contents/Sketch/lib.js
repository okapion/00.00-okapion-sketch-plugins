var com = com || {};

com.okapion = {

  init: function (context) {
    com.okapion.context = context;
    com.okapion.doc = context.document;
    com.okapion.selection = context.selection;
    com.okapion.log = {};
  },

  /**
   * Returns true if the given string is a valid css selector
   * currently used to check naming conventions with layers
   */
  isValidCss: function isValidCss(str) {
    return /\.(-?[_a-zA-Z]+[_a-zA-Z0-9-]*)(?![^\{]*\})/.test(str);
  },

  /**
   * Returns true if the layer is a layer we can manager
   */
  isLayer: function(layer) {
    var className = layer.className();
    return (!isSymbolInstance(layer) && className != 'MSPage' && className != 'MSArtboardGroup');
  },

  /**
   * Returns true if the given layer is related to a symbol
   */
  isSymbolInstance: function(layer) {
    return (layer.className() == 'MSSymbolInstance');
  },

  /**
   * Returns true if the given layer is a symbol
   */
  isSymbolMaster: function(layer) {
    return (layer != null && layer.className() == 'MSSymbolMaster');
  },

  /**
   * Returns true if the given page is the special Symbols page
   */
  isSymbolsPage: function (page) {
    var layers = page.children().objectEnumerator();

    while(layer = layers.nextObject()) {
      if (com.okapion.isSymbolMaster(layer)) {
        return true;
      }
    };

    return false;

    // BSM 27-03-2019
    // We changed from name based, to checking each page and seeing if
    // it contains any master. If it does, we assume it's a symbols page.
    // return page.name().indexOf('Symbols') > -1;
  },

  /**
   * Returns a reference to the document's symbols page
   */
  getSymbolsPage: function(context) {
	   var pages = context.document.pages();

  		// Get index of Symbols page
  		for (var i = 0; i < pages.count(); i++) {
  			if (pages.objectAtIndex(i).name() == "Symbols") {
  				context.document.setCurrentPage(i);
  				return context.document.currentPage();
  			}
  		}
  },

  /**
   * If the give layer is linked to a symbol, this shortcut will help
   * you get the name of that related symbol.
   */
  getSymbolMaster: function(layer) {
    return layer.symbolMaster().parentSymbol();
  },

  /**
   * Update the given layer's name, take note that this will overwrite
   * any tags encoded in the layer name.
   */
  setLayerName: function(layer, name) {
    [layer setName:name];
  },

  /**
   * Create new page in the doc
   */
  addPage: function(name) {
    var page = com.okapion.doc.addBlankPage();
    page.setName(name);
    com.okapion.doc.setCurrentPage(page);
    return page;
  },

  /**
   * Create new artboard in the given page, with the given name
   */
  addArtboard: function(page, name) {
    var artboard = MSArtboardGroup.new();
    frame = artboard.frame();
    frame.setWidth(400);
    frame.setHeight(1000);
    frame.setConstrainProportions(false);
    page.addLayers([artboard]);
    artboard.setName(name);
    return artboard;
  },

  /**
   * Create new textlayer to the given object, with the given name
   */
  addTextLayer: function(target, label) {
    var textLayer = target.addLayerOfType("text");
    textLayer.setStringValue(label)
    textLayer.setName(label)
    return textLayer;
  },

  /**
   * Returns the page if it exists or creates a new page
   */
  getPageByName: function(name) {
    for (var i = 0; i < com.okapion.doc.pages().count(); i++) {
      var page = com.okapion.doc.pages().objectAtIndex(i);
      if (page.name() == name) {
        com.okapion.doc.setCurrentPage(page);
        return page;
      }
    }
    var page = com.getflourish.common.addPage(name);
    return page;
  },


  /**
   * Given the layer, returns the rect data
   */
  getRect: function(layer) {
    var rect = layer.absoluteRect();
    return {
        x: Math.round(rect.x()),
        y: Math.round(rect.y()),
        width: Math.round(rect.width()),
        height: Math.round(rect.height()),
        maxX: Math.round(rect.x() + rect.width()),
        maxY: Math.round(rect.y() + rect.height()),
        setX: function(x){ rect.setX(x); this.x = x; this.maxX = this.x + this.width; },
        setY: function(y){ rect.setY(y); this.y = y; this.maxY = this.y + this.height; },
        setWidth: function(width){ rect.setWidth(width); this.width = width; this.maxX = this.x + this.width; },
        setHeight: function(height){ rect.setHeight(height); this.height = height; this.maxY = this.y + this.height; }
    };
  },

  /**
   * Test to help find duplicate symbol names
   */
  findDuplicates: function(input) {
    var duplicates = [], i, j;
    for (i = 0, j = input.length; i < j; i++) {
      if (duplicates.indexOf(input[i]) === -1 && input.indexOf(input[i], i+1) !== -1) {
        duplicates.push(input[i]);
      }
    }
    return duplicates;
  },

  /**
   * Opens file browser, returns directory path
   */
  browseForDirectory: function() {
    var path;

    // Create select folder window
    var panel = [NSOpenPanel openPanel];
    [panel setCanChooseDirectories:true];
    [panel setCanCreateDirectories:true];

    var clicked = [panel runModal];

    // Check if Ok has been clicked
    if (clicked == NSFileHandlingPanelOKButton) {
        var isDirectory = true;
        // Get the folder path
        var firstURL = [[panel URLs] objectAtIndex:0];
        // Format it to a string
        path = [NSString stringWithFormat:@"%@", firstURL];

        // Remove the file:// path from string
        if (0 === path.indexOf("file://")) {
            path = path.substring(7);
        }
    }

    return path;
  },

  /**
   * Receives a string, and tries a lot of ways to make it return a valid
   * css class selector name (including the dot). Bit brute force this one.
   */
  makeCssSelector: function(str) {

    str = str.toLowerCase();

    // Remove spaces
    str = str.replace(/ /g, '');

    // Replace all slashes with -
    str = str.replace(/[/]/g, '-');
    str = str.replace(/[\\]/g, '-');

    // Replace all () with _
    str = str.replace('(', '_');
    str = str.replace(')', '_');

    // Replace any existing dots
    str = str.replace('.', '');

    // Add the dot and a specific string (so we don't get css errors if
    // the css string starts with a number). and return.
    return '.s-' + str;
  },

  /**
   * Receives a full path plus filename, writes content to it
   */
  writeToFile(filename, content) {
    var t = [NSString stringWithFormat:@"%@", content];
    [t writeToFile:filename atomically:true encoding:NSUTF8StringEncoding error:null];
  },

  // Test
  // @param object orig    the object to extend
  // @param array keyParts the.key.path split by "." (expects an array, presplit)
  // @param mixed value    the value to assign
  // @param object scoped  used by the recursion, ignore or pass null
  makeTree: function(orig, keyParts, value, scoped) {
      if (!scoped) {
          scoped = orig;
      }

      var nextKey = keyParts.shift();

      if (keyParts.length === 0) {
          scoped[nextKey] = {
            value: value,
            children: {}
          }
          return orig;
      }

      if (!scoped[nextKey]) {
          scoped[nextKey] = {
            value: null,
            children: {}
          };
      }

      scoped = scoped[nextKey].children;
      return this.makeTree(orig, keyParts, value, scoped);
  },
};
