@import 'lib.js'

var onRun = function(context) {

  var doc = context.document;

  com.okapion.init(context);

  // Cleanup the naming of all Symbol instances on pages
  doExtractCSS(context);

  // All done!
  doc.showMessage("Export done");
}

/**
 * Finds all pages and cleans up each one of them
 */
function doExtractCSS(context) {
  var doc = context.document;

  // The final scss
  var output = '';

  // Where to save the file (css)
  var path = com.okapion.browseForDirectory();

  // Loop through all symbols to get their definition
  var symbols = doc.documentData().allSymbols().objectEnumerator();

  while(symbol = symbols.nextObject()) {

    var symbolName = com.okapion.makeCssSelector(symbol.name());

    // Open the scss selector
    output += symbolName + ' {' + '\n';

    // Get all the layers that make the symbol.
    var layers = symbol.layers().objectEnumerator();

    while(layer = layers.nextObject()) {

      // Do nothing if the layer is a group, it will be empty. Also do
      // not use sub-symbols, since they will be handled when we get
      // there on it's own definition.
      if (layer.className() != 'MSLayerGroup' && !com.okapion.isSymbolInstance(layer)) {
        var layerName = com.okapion.makeCssSelector(layer.name());

        // Open the scss selector
        output += '\t' + layerName + ' {\n';

        // Add css props, by getting the css object, and looping
        // through it per line adding to the output
        var attrs = layer.CSSAttributes();

        for(var i=0; i< attrs.count(); i++) {

          // Check that we don't add any comment lines to the output
          if (attrs[i].indexOf('/*') == -1) {
            output += '\t\t' + attrs[i].toLowerCase() + '\n';
          }
        }

        // Close the scss selector
        output += '\t}\n\n';
      }
    }

    // Close the scss selector
    output += '}\n\n';

    try {
    } catch (e) {log("error");}
  }

  // write to text file
  com.okapion.writeToFile(path + '/extract.css', output);
};
