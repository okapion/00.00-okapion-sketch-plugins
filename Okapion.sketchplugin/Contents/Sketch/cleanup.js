@import 'lib.js'

var onRun = function(context) {

  var doc = context.document;

  com.okapion.init(context);

  // Cleanup the naming of all Symbol instances on pages
  doCleanupAllPages(context);

  // Log's the duplicate symbol definitions to console.
  // doCheckForSymbolDuplicates(context);

  // All done!
  doc.showMessage("Cleanup done");
}


/**
 * Finds all pages and cleans up each one of them
 */
function doCleanupAllPages(context) {
  var pages = context.document.pages().objectEnumerator();

  // Cleans up the symbol instance names of all pages
  while(page = pages.nextObject()) {

    var isSymbolsPage = com.okapion.isSymbolsPage(page);

    var layers = page.children().objectEnumerator();

    while(layer = layers.nextObject()) {

      try {
        var name = layer.name();

        // Check if this layer represents an instance of a master symbol on the symbol page
        // This script only renames symbol instances, so if we're not dealing with one,
        // we can fully ignore it.
        if (com.okapion.isSymbolInstance(layer)) {
          var renamedChildSubSymbol = false;

          // We handle the symbols page differently, as in... if we have sub symbols
          // there, we might have renamed them intentionally to be able to make them
          // more useable in the rest of the file. E.g., we might rename something
          // to 'label', because it then pops up as such in the properties panel.
          if (isSymbolsPage) {
            name = layer.name().replace(/\s*\(.*?\)\s*/g, '');

            // Did we rename it? We assume if the name does not contain any forward
            // slashes, we must have renamed it. This is not bulletproof though!
            renamedChildSubSymbol = name.indexOf('/') === -1;
          }

          // Only if we didn't rename it, do we rename now.
          if (renamedChildSubSymbol) {
            name = name + ' (' + com.okapion.getSymbolMaster(layer).name() + ')';
          } else {
            // No special treatment needed, just reset the name of the instance to match
            // the name that is on the master symbol on the symbols page.
            name = com.okapion.getSymbolMaster(layer).name();
          }
        } else {
          name = cleanupLayerName(name);
        }

        // After all decided, set this new layer named
        com.okapion.setLayerName(layer, name);
      } catch (e) {log("error");}
    }
  }
}

/**
 * Pass this the symbols page reference, will check if there are
 * any duplicate artboard names.
 */
function doCheckForSymbolDuplicates(context) {
    // The easy way to get iterator over all symbols in doc
    var symbols = context.document.documentData().allSymbols().objectEnumerator();

    var names = [];
    while(symbol = symbols.nextObject()) {
      names.push(symbol.name());
    }

    var duplicates = com.okapion.findDuplicates(names);

    if (duplicates.length == 0) {
      log(duplicates);
      log('Found: ' + duplicates.length + ' duplicates');
    }
}

/**
 * A function that tries to clean up the strings, matching the rules we came with
 * as a team @ Okapion
 */
function cleanupLayerName(str) {

  // If a layer is named "bg", rename it to "Background"
  if (str.toLowerCase() == 'bg')
    str = 'Background';

  // If it has "group" in it as it's only name, assume it's automatically named and use 'g'
  if (str.toLowerCase().indexOf('group') == 0)
    str = 'g';

  // Capitalize first letter, if not a single letter
  if (str.length > 1)
    str = str.charAt(0).toUpperCase() + str.slice(1);

  return str;
}
